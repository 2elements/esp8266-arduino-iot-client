//#include <ESP8266WiFi.h>          //ESP8266 Core WiFi Library (you most likely already have this in your sketch)
#include <DNSServer.h>            //Local DNS Server used for redirecting all requests to the configuration portal
#include <ESP8266WebServer.h>     //Local WebServer used to serve the configuration portal
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager WiFi Configuration Magic
#include <ESP8266mDNS.h>
//#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>
#include <ESP8266httpUpdate.h>


#define RELAY1 D2
#define RELAY2 D3
#define RELAY3 D4


const String VERSION = "2";

//flag for saving data
bool shouldSaveConfig = false;
//const String UNIQUE_ID = "1562-9081-18263";
const String UNIQUE_ID = String(ESP.getChipId());
String PROTO = "http";
String HOST = "139.59.49.43";
String PORT = "3000";
String updateURI = "/updates/" + UNIQUE_ID + "?version=" + VERSION;
String S_URL = PROTO + "://" + HOST + ":" + PORT;


void setup() {
  // put your setup code here, to run once:

  Serial.begin(115200);
  Serial.setDebugOutput(true);
  pinMode(RELAY1, OUTPUT);
  pinMode(RELAY2, OUTPUT);
  pinMode(RELAY3, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);

  Serial.println(UNIQUE_ID);

  setupWifi();

  //check for OTA updates
  otaUpdater();
  
  //register device to the server
  registerDevice();
  delay(3000);
  
  Serial.println("Setup Complete");

}

void setupWifi(){
  WiFiManager wifiManager;

  wifiManager.setBreakAfterConfig(true);
  //wifiManager.autoConnect();
  wifiManager.setAPCallback(configModeCallback);
  wifiManager.setSaveConfigCallback(saveConfigCallback);
  wifiManager.setConfigPortalTimeout(180); //to prevent hanging

  if (!wifiManager.autoConnect()) {
    Serial.println("failed to connect, we should reset as see if it connects");
    delay(3000);
    ESP.reset();
    delay(5000);
  }

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println("...waiting for WiFi");
  }
  Serial.println("");
  Serial.println("WiFi connected");
}


void configModeCallback (WiFiManager *myWiFiManager) {
  Serial.println("Entered config mode");
  myWiFiManager->resetSettings();
  Serial.println(WiFi.softAPIP());
  Serial.println(myWiFiManager->getConfigPortalSSID());
}

//callback notifying us of the need to save config
void saveConfigCallback () {
  Serial.println("Should save config");
  Serial.println(WiFi.localIP());
  shouldSaveConfig = true;
}



void otaUpdater(){
  Serial.println("[update] Checking for updates...");
  t_httpUpdate_return ret = ESPhttpUpdate.update(S_URL + updateURI);
  switch(ret) {
    case HTTP_UPDATE_FAILED:
        Serial.println("[update] Update failed.");
        break;
    case HTTP_UPDATE_NO_UPDATES:
        Serial.println("[update] Update no Update.");
        break;
    case HTTP_UPDATE_OK:
        Serial.println("[update] Update ok."); // may not called we reboot the ESP
        break;
  }
}


void registerDevice(){
  if(WiFi.status()== WL_CONNECTED){   //Check WiFi connection status
 
   HTTPClient http;    //Declare object of class HTTPClient
   StaticJsonBuffer<300> JSONbuffer;
   JsonObject& JSONencoder = JSONbuffer.createObject();   

   JSONencoder["name"] = "Node MCU";
   JSONencoder["unique_id"] = UNIQUE_ID;
   JSONencoder["total_devices"] = "3";
   char JSONmessageBuffer[300];
   JSONencoder.prettyPrintTo(JSONmessageBuffer, sizeof(JSONmessageBuffer));
   Serial.println(JSONmessageBuffer);

   String url = S_URL + "/node/register";
   http.begin(url);      //Specify request destination
   http.addHeader("Content-Type", "application/json");  //Specify content-type header
 
   int httpCode = http.POST(JSONmessageBuffer);   //Send the request
   String payload = http.getString();                  //Get the response payload
 
   Serial.println(httpCode);   //Print HTTP return code
   Serial.println(payload);    //Print request response payload
 
   http.end();  //Close connection
 
 }else{
 
    Serial.println("Error in WiFi connection");   
 
 }
}


void getDevicesState(){
  if(WiFi.status()== WL_CONNECTED){   //Check WiFi connection status
 
   HTTPClient http;    //Declare object of class HTTPClient
   StaticJsonBuffer<300> JSONbuffer;
   JsonObject& JSONencoder = JSONbuffer.createObject();   

   String url = S_URL + "/node/" + UNIQUE_ID;
   http.begin(url);      //Specify request destination
   http.addHeader("Content-Type", "application/json");  //Specify content-type header
 
   int httpCode = http.GET();   //Send the request
   String payload = http.getString();                  //Get the response payload

   //const size_t bufferSize = 2000;
   StaticJsonBuffer<2000> jsonBuffer;
   JsonObject& resp = jsonBuffer.parseObject(payload);
   //JsonObject& devices1;
   //JsonObject devices2;
   //JsonObject devices3;
   if (!resp.success()) {
    Serial.println(F("Parsing failed!"));
    Serial.println(httpCode);
    Serial.println(payload);
    return;
  }

   long code = resp["code"];
   if(code == 1){
    Serial.println("Correct Data...");
    //devices1 = resp["data"]["devices"][0];
    //devices2 = resp["data"]["devices"][1];
    //devices3 = resp["data"]["devices"][2];

    for(int i = 0 ; i<3; i++){
      int id = resp["data"]["devices"][i]["id"];
      int state = resp["data"]["devices"][i]["state"];
      Serial.println("Id: " + String(id) + " State: " + String(state));
      controlPin(id, state);
    }
    
   }else{
    Serial.print("Something is wrong...");
    Serial.println(httpCode);   //Print HTTP return code
    Serial.println(payload);
   }
   
   http.end();  //Close connection
 
 }else{
 
    Serial.println("Error in WiFi connection");   
 
 }
}

void controlPin(int id, int state){
  
  switch(id){
    case 1: digitalWrite(RELAY1, state == 1 ? LOW : HIGH);
      break;
    case 2: digitalWrite(RELAY2, state == 1 ? LOW : HIGH);
      break;
    case 3: digitalWrite(RELAY3, state == 1 ? LOW : HIGH);
     break;
  }
}


void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(LED_BUILTIN, HIGH);
  getDevicesState();
  digitalWrite(LED_BUILTIN, LOW);
  delay(500);
}
